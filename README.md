Structure de la table TLOG_MESSAGES :
___
<pre>
[TLOG_MESSAGES](
    [ID] [int] IDENTITY(1,1) NOT NULL,
    [JCRE] [datetime] NOT NULL,
    [LOG_LEVEL] [int] NOT NULL,
    [ORIGIN] [varchar](8000) NULL,
    [SCOPE] [varchar](400) NULL,
    [LOGUSER] [varchar](8000) NULL,
    [DOMAINE] [varchar](8000) NULL, NB: Un ID est enregistre meme si le type est varchar ...
    [MESSAGE_NUMBER] [bigint] NOT NULL,
    [LOG_MESSAGE] [varchar](max) NULL,
    [OTHERSINFOS] [varchar](max) NULL,
)
</pre>

Details des proprietes :
___
<pre>
<b>ID =</b> Identifiant du tuple

<b>JCRE =</b> Date de creation du log 

<b>LOG_LEVEL =</b> Niveau de criticite du log 
    Herited = -1,
    Debug = 0,
    Info = 1,
    Notice = 2,
    Warning = 3,
    Error = 4,
    Critical = 5

<b>ORIGIN =</b> Application a l'origine du log (ou a defaut, contexte applicatif) ex: Track, ERD, Centralisation, migration de donnees ...

<b>SCOPE =</b> Contexte d'enregistrement du log :
    Nom de fonction ex:GetValue()
    Nom de procedure stockee ex:pr_TProtocole_ExportOneLight
    (ou autre, a defaut de, doit permettre de localiser d'ou provient le log) ex: "Automatic Script Execution", "ViewProtocoleEditField.aspx"

<b>LOGUSER =</b> Nom de l'utilisateur a l'origine du log ou ID de l'utilisateur (dans SECURITE), ou a defaut, nom de l'utilisateur BDD ou Windows (NULL accepte egalement)

<b>DOMAINE =</b> Entite a l'origine du log
    DataBase = 0,
    ClientAplication = 1,
    WebApplication = 2,
    WebService = 3,
    External = 4,
    OS = 5,
    Command = 6,
    Ressources = 7,
    BusinessGlobalUnknown = 8

<b>MESSAGE_NUMBER =</b> Le numero associe au log, laisser a 1 si non adapte au contexte, sinon mettre une valeur pertinente, ex: le numero retourne par l'appel http: 200, 401, 500 ...

<b>LOG_MESSAGE =</b> Le message lie au log (essentiellement decrit une action ou le resultat d'une action)

<b>OTHERSINFOS =</b> Informations utiles permettant de donner un contexte au log, ex: parametres fournis a l'appel d'une fonction de code ou d'une procedure stockee en BDD

</pre>
